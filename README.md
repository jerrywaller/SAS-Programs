# SAS-Programs
if I had a nickel for every github comment...

# License
*This software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.*

_Yes!_ You too may be able to use SAS for little to no cost. Check here:
SAS University Edition: http://www.sas.com/en_us/software/university-edition.html

These are SAS files for academic and public libraries to use/copy/modify/inspire.

github's sas syntax actually works when you don't upload your program as a binary file. :/
