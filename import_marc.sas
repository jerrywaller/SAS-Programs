/*Import tab-delimited mnemonic marc records
	from OpenRefine. All variables are brought
	in as text.*/


data marc_in;
	length key $ 10
			tag $ 4
			field $ 2
			data $ 3500;
	infile "C:\Users\jwaller7\Documents\OneDrive for Business\backToMac\lita_demo_1.tsv"
		dlm='09'x firstobs=2 missover dsd lrecl=3500; *this line probably needs tweaked;
	input key $ tag $ field $ data $;
run;

/*Create a numeric variable based upon the 'key' variable.*/
data alpha;
	set marc_in;
	if key ne '' then marc_id=input(key,best.);
		else if key = '' then marc_id=.;
	if key ne '' then n_key=input(key,best.);
run;

/*Enumerate each line of the individual marc record and
	drop the character key variable.*/

data beta (drop=key id);
	retain n_key marc_id;
	set alpha;
	if marc_id ne . then 
		do;
			id+1;
		end;
	marc_id=id;
run;


/************************finis**********************************/
