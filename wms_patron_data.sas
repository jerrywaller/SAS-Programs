data alpha;
		length 	name $ 200
				birth $ 200
				datatel $ 200
				barcode $ 200
				email $ 1000
				ptype $ 1000
				created $ 1000
				expire $ 200
				branch $ 200
				address $ 1000 
				phone $ 200
				email2 $ 1000 
				pmessage $ 2000 
				note $ 2000
				precord $ 1000;
	infile "C:\Users\jwaller7\Documents\OneDrive for Business\backToMac\wms\wms_patron_raw.tsv" 
		dlm='09'x firstobs=2 missover dsd; 
	input name $ birth $ datatel $ barcode $ email $ ptype $ created $ expire $ branch $ address $ phone $ email2 $ pmessage $ note $ precord $;

run;

data bravo (drop=pmessage);
	set alpha;
	name=trim(name); 
	birth=compress(birth);
	datatel=compress(datatel);
	barcode=compress(barcode);
	email=compress(email); 
	ptype=compress(ptype); 
	created=compress(created); 
	expire=compress(expire); 
	branch=compress(branch); 
	address=trim(address);  
	phone=compress(phone);  
	email2=compress(email2);  
	pmessage=trim(pmessage);   
	note=trim(note);  
	precord=compress(precord);
run;

data charlie (drop= birth year month day);
	set bravo;
	namebreak=index(name,',');
	n=length(name);
	lname=trim(scan(name,1,','));
	fm_name=substr(name,(namebreak+1),(n-namebreak));
	year=substr(birth,7,10);
	month=substr(birth,1,2);
	day=substr(birth,4,2);
	dob=(compress(year)||'-'||compress(month)||'-'||compress(day));
run;

data delta (drop=name namebreak n fm_name email);
	retain fname mname lname dob;
	set charlie;
	fname=scan(name,2,' ');
	mname=scan(name,-1,' ');
	lname=scan(name,1,',');
	if fname=mname then mname='';
run;

data echo;
	set delta;
	if ptype='0' then ptype='undergraduate';
	else if ptype='1' then ptype='graduate';
	else if ptype='2' then ptype='faculty/staff';
	else if ptype='3' then ptype='retired faculty/staff';
	else if ptype='4' then ptype='friend of the library';
	else if ptype='5' then ptype='family';
	else if ptype='7' then ptype='auxiliary';
	else if ptype='8' then ptype='courtesy';
	else if ptype='9' then ptype='visiting scholar';
	else if ptype='10' then ptype='TALA';
	else if ptype='11' then ptype='interlibrary loan';
	else if ptype='12' then ptype='course reserves';
	else if ptype='13' then ptype='ELON Academy';
	else if ptype='15' then ptype='Law ILL';
	else if ptype='16' then ptype='armc';
	else if ptype='255' then ptype='negative patron';
run;

data foxtrot (drop=cyear cmonth cday eyear emonth eday);
	set echo;
	cyear=substr(created,7,10);
	cmonth=substr(created,1,2);
	cday=substr(created,4,2);
	eyear=substr(expire,7,10);
	emonth=substr(expire,1,2);
	eday=substr(expire,4,2);
	created=(compress(cyear)||'-'||compress(cmonth)||'-'||compress(cday)||'T23:59:59');
	expire=(compress(eyear)||'-'||compress(emonth)||'-'||compress(eday)||'T23:59:59');
run;

data golf;
	set foxtrot;
	if branch='1b' then branch='215495';
	else if branch='2l' then branch='215497';
	if address='' then address='null';
run;

data hotel (drop=citystatezip statezip stbreak n);
	set golf;
	stbreak=index(address,'$');
	n=length(address);
	street=scan(address,1,'$');
	if stbreak > 1 then 
		citystatezip=scan(address,2,'$');
		else citystatezip='null';
			if compress(citystatezip) eq '' then citystatezip='null';
	if citystatezip ne 'null' then
		city=scan(citystatezip,1,',');
		else city='null';
	if citystatezip ne 'null' then
		statezip=scan(citystatezip,2,',');
		else statezip='null';
			if compress(statezip)='' then statezip='null';
	if statezip ne 'null' then 
		state=scan(statezip,1,' ');
		else state='null';
			if compress(state)='' then state='null';
	if statezip ne 'null' then
		zip=scan(statezip,2,' ');
		else zip='null';
			if compress(zip)='' then zip='null';
run;

data india (drop=address);
	retain fname mname lname dob datatel barcode ptype created expire branch street city state zip phone email2 note;
	set hotel;
	if compress(datatel)='' then delete;
	if datatel='DATATELID' then delete;
	if dob='--' then dob='1901-01-01';
	if compress(barcode)='' then barcode='1010101010101';
	if street='null' then street='Campus Box';
	if compress(street)='' then street='Campus Box';
	if city='null' then city='Elon';
	if state='null' then state='NC';
	if zip='null' then zip='27244';
	if compress(phone)='' then phone='111-111-1111';
	note=substr(note,1,254);
run;

data z (keep=prefix givenName middleName familyName suffix nickname dateOfBirth gender institutionId bbarcode idAtSource sourceSystem borrowerCategory circRegistrationDate circExpirationDate homeBranch primaryStreetAddressLine1 primaryStreetAddressLine2 primaryCityOrLocality primaryStateOrProvince primaryPostalCode primaryCountry primaryPhone secondaryStreetAddressLine1 secondaryStreetAddressLine2 secondaryCityOrLocality secondaryStateOrProvince secondaryPostalCode secondaryCountry secondaryPhone emailAddress mobilePhone patronNotes photoURL customdata1 customdata2 customdata3 customdata4);
	set india;
	prefix =' ';
	givenName = fname;
	middleName = mname;
	familyName = lname;
	suffix =' ';
	nickname =' ';
	dateOfBirth = dob;
	gender =' ';
	institutionId = datatel;
	bbarcode = barcode;
	idAtSource =' ';
	sourceSystem =' ';
	borrowerCategory = ptype;
	circRegistrationDate = created;
	circExpirationDate = expire;
	homeBranch = branch;
	primaryStreetAddressLine1 = street;
	primaryStreetAddressLine2 =' ';
	primaryCityOrLocality = city;
	primaryStateOrProvince = state ;
	primaryPostalCode = zip;
	primaryCountry =' ';
	primaryPhone = phone;
	secondaryStreetAddressLine1 =' ';
	secondaryStreetAddressLine2 =' ';
	secondaryCityOrLocality =' ';
	secondaryStateOrProvince =' ';
	secondaryPostalCode =' ';
	secondaryCountry =' ';
	secondaryPhone =' ';
	emailAddress = email2;
	mobilePhone =' ';
	patronNotes = note;
	photoURL =' ';
	customdata1 =' ';
	customdata2 =' ';
	customdata3 =' ';
	customdata4 =' ';
run;


PROC EXPORT DATA= WORK.Z 
            OUTFILE= "C:\Users\jwaller7\Documents\OneDrive for Business\
backToMac\wms\patron_data_file.txt" 
            DBMS=TAB LABEL REPLACE;
     PUTNAMES=YES;
RUN;
